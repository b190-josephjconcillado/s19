// console.log("Hello World!");


// if statement
    // -executes a statement if a specified condition is true
    // -can stand alone without the else statement
    // syntax:
    //     if(condition){
    //         statement/code block
    //     }

let numA = -1;

if(numA < 0){
    console.log("Hello!");
}

if(numA > 0){
    console.log("This statement will not be printed");
}

let city = "New York";
if(city === "New York"){
    console.log("Welcome to "+city+ " City!");
}

// else if clause
    // - executes a statement if previous conditions are false and if the specified condition is true
    // - "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

let numB = 1;
if (numA > 0){
    console.log("Hello!");
} else if (numB > 0){
    console.log("World");
}

if (numA < 0){
    console.log("Hello!");
} else if (numB > 0){
    console.log("World");
}

city = "Tokyo";
if(city === "New York"){
    console.log("Welcome to New York City!");
}
else if(city === "Tokyo"){
    console.log("Welcome to Tokyo!");
}

// else statement
    // -it executes a statement if all other conditions are false
    // -the 'else' statement is optional and can be added to capture any other result to change the flow of a program

if(numA > 0){
    console.log("Hello");
}
else if(numB === 0){
    console.log("World");
}
else{
    console.log("Again");
}
// let age = prompt("Enter your age.")
// if(age <= 18){
//     console.log("Not allowed to drink!");
// }else{
//     console.log("Matanda ka na, shot na!");
// }
// console.log("------");

// Mini Activity: Create a condition statement if the height is below 150, display "Did not passed the minimum requrement". if above 150, display "passed the minimum height requirement"

// function checkHeight(){
//     let height = prompt("Enter your height.");
//     console.log("Height: "+height+ "cm");
//     if(height < 150){
//         console.log("You did not pass the minimum height requirement.");
//     }else if(height >= 150){
//         console.log("You passed the minimum height requirement.")
//     }
// }
// checkHeight();

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed){
    if(windSpeed < 30) {
        return 'Not a typhoon yet';
    } else if(windSpeed <= 61) {
        return 'Tropical depression detected.'
    } else if (windSpeed >= 62 && windSpeed <= 88){
        return 'Tropical storm detected.'
    } else if( windSpeed >= 89 && windSpeed <= 117){
        return 'Severe tropical storm detected.'
    } else {
        return 'Typhoon detected.'
    }
}
message = determineTyphoonIntensity(69);
console.log(message);

if(message == "Tropical storm detected."){
    console.warn(message);
}

//Truthy and Falsy
    // Truthy
    // -In javascript, a truthy value is a value that is considered true when encountered in a boolean context
    // -Values are considered true unless defined otherwise.
    // Falsy
    // -values / exceptions for truthy
    // 1. false
    // 2. 0
    // 3. -0
    // 4. ""
    // 5. null
    // 6. undefined
    // 7. NaN

// Truthy Example
if(true){
    console.log("Truthy!")
} if(1){
    console.log("Truthy!")
} if([]){
    console.log("Truthy!")
}

//Falsy Examples
if(false){
    console.log("Falsy!");
} if(0){
    console.log("Falsy!");
} if(undefined){
    console.log("Falsy!");
}

//Conditional (Ternary operator)
    // the ternary operator takes in three operands 
    // 1. condition 
    // 2. expression to execute if the condition is truthy
    // 3. expression to execute if the condition is falsy

    // Syntax:
    //     Condition ? ifTrue : ifFalse

//Single statment execution
// let ternaryResult = (1 < 18) ? "gwapo" : "Pangit";
// console.log("Result of ternary operator: "+ternaryResult);

// // let names;

// function isOfLegalAge(){
//     names ='John';
//     return 'Your are of the legal age limit.';
// }
// function isUnderAge(){
//     names = "Jane" ;
//     return 'You are under the age limit';
// }

// let age = parseInt(prompt("What is your age?"));
// console.log(age);
// let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary Operator in Functions: "+legalAge+ ", "+ names);

// Switch statment
    // can also be used as an alternative with if and else statement
    // syntax:
    //     switch (expression) {
    //         case value : 
    //             statement;
    //             break;
    //         default
    //             statement;
    //             break;
    //     }

// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

//     switch(day){
//         case 'monday':
//             console.log("The color of the day is red");
//             break;
//         case 'tuesday':
//             console.log("The color of the day is yellow");
//         case 'wednesday':
//             console.log("The color of the day is green");
//         case 'thursday':
//             console.log("The color of the day is blue");
//         case 'friday':
//             console.log("The color of the day is indigo");
//         case 'saturday'||'sunday':
//             console.log("The color of the day is violet");
//             break;
//         // case 'sunday':
//         //     console.log("The color of the day is orange");
//         default:
//             console.log("Please input a valid day!");
//             break;
//     }
    
function showIntensityAlert(windSpeed){
    try{
        alert(determineTyphoonIntensity(windSpeed));
    }

    catch(error){
        console.log(error);
        console.warn(error.message);
    }
    finally{
        alert("Intensity updates will show new alert.")
    }
}
showIntensityAlert(56);